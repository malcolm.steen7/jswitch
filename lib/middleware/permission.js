/**
 * Generic permission middleware.
 * This is an extension of the auth middleware, so routes must not use both of them.
 */

// local modules
const { respond } = require("../util");

module.exports = function(server) {
  const {
    providers,
    db: { models }
  } = server;
  const logger = server.logger.sub("mw.permission");

  // main permission function that returns a middleware function
  const perm = function(provider, name, permitOnMissing = false) {
    return async function(ctx, next) {
      const fail = () => respond(ctx, null, 403, "Forbidden.");

      const user =
        Number.isInteger(ctx.session.userId) &&
        (await models.User.getById(ctx.session.userId));

      // does the user exist?
      if (!user) {
        logger.debug("user does not exist");
        return fail();
      }
      ctx.user = user;

      // when the provider does not exist, "permitOnMissing" decides what is happening
      if (providers[provider] === undefined) {
        if (permitOnMissing) {
          await next();
        } else {
          logger.debug(
            `provider ${provider} not configured and no permitOnMissing`
          );
          fail();
        }
      } else if (!(await models.Permission.exists(user, provider, name))) {
        // does the user have the permission?
        logger.debug(
          `user ${user.name} not permitted acccess to ${provider}.${name}`
        );
        fail();
      } else {
        await next();
      }
    };
  };

  // returns a new permission function that prefixes all permission names
  perm.sub = function(prefix) {
    return function(provider, name, ...args) {
      return perm(provider, prefix + name, ...args);
    };
  };

  return perm;
};
