/**
 * Main server class.
 */

// node modules
const fs = require("fs");
const path = require("path");
const http = require("http");
const https = require("https");

// npm modules
const { EventEmitter2 } = require("eventemitter2");
const winston = require("winston");
const Koa = require("koa");
const kCompress = require("koa-compress");
const kMorgan = require("koa-morgan");
const kRouter = require("koa-router");
const kViews = require("koa-views");
const kSession = require("koa-session");
const kBodyParser = require("koa-bodyparser");
const kValidate = require("koa-validate");
const SocketIO = require("socket.io");
const deepExtend = require("deep-extend");
const uuidv4 = require("uuid/v4");

// local modules
const util = require("./util");
const Events = require("./events");
const Hooks = require("./hooks");
const DB = require("./db");
const staticMW = require("./middleware/static");
const userMW = require("./middleware/user");
const basicAuthMW = require("./middleware/basicauth");
const permMW = require("./middleware/permission");
const catchMW = require("./middleware/catch");

/**
 * Class definition.
 */

class Server extends EventEmitter2 {
  constructor(args) {
    super({ maxListeners: 50 });

    // components
    this.args = args;
    this.events = new Events(this);
    this.hooks = new Hooks(this);
    this.config = null;
    this.logger = null;
    this.db = null;
    this.app = null;
    this.router = null;
    this.server = null;
    this.socket = null;
    this.info = {};
    this.middleware = {};
    this.providers = {};

    // bind shutdown events
    for (const event of ["SIGTERM", "SIGINT", "SIGQUIT"]) {
      process.on(event, async () => await this.shutdown(event, 0));
    }
    for (const event of ["unhandledRejection"]) {
      process.on(event, async err => await this.shutdown(event, 1, err));
    }
  }

  async setup(opts = {}) {
    const { configOpts = {} } = opts;

    await this.hooks.call("server.setup.before");

    await this.setupConfig(configOpts);
    this.setupLogging();
    await this.setupEvents();
    await this.setupHooks();
    await this.setupDB();
    await this.setupMiddleware();
    await this.setupProviders();
    await this.setupBundling();
    await this.setupApp();
    await this.setupSocket();

    await this.hooks.call("server.setup.after");
  }

  async shutdown(msg = "-", code = 0, err = null) {
    msg = `initiate shutdown due to: ${msg}`;
    const logger = this.logger || console;
    const level = code <= 0 ? "info" : (this.logger && "fatal") || "error";
    logger[level](msg);

    if (err) {
      logger[level](err);
      logger[level](err.stack);
    }

    // shutdown components
    if (this.db) {
      await this.db.shutdown();
    }
    logger.info("server gracefully shutdown");

    // stop the process when the exit code is not null
    if (code !== null) {
      logger.info(`stop server process with exit code ${code}`);
      process.exit(code);
    }
  }

  async listen() {
    const c = this.config.server;

    await this.hooks.call("server.listen.before");
    this.server.listen(c.port, c.host);
    await this.hooks.call("server.listen.after");

    // store the local http(s) address
    const scheme = this.info.sslEnabled ? "https" : "http";
    this.info.address = `${scheme}://${c.host}:${c.port}${c.base}`;

    this.logger.info(`server listening at ${this.info.address}`);
  }

  async setupConfig({ sample = "config.yml" }) {
    // read the sample config to ensure that every entry is present
    this.config = util.readConfigSync(`conf/samples/${sample}`);

    // extend it by a custom config file
    const files = [
      this.args.configFile,
      process.env.JSWITCH_CONFIG,
      `conf/${sample}`
    ];
    for (const file of files) {
      if (fs.existsSync(file)) {
        console.info(`loading config from file ${file}`);
        deepExtend(this.config, util.readConfigSync(file));
        break;
      }
    }

    // overwrite with some cli args
    if (this.args.host !== null) {
      this.config.server.host = this.args.host;
    }
    if (this.args.port !== null) {
      this.config.server.port = this.args.port;
    }
    if (this.args.base !== null) {
      this.config.server.base = this.args.base;
    }
    if (this.args.logLevel !== null) {
      let consoleTransports = this.config.logging.transports.Console;
      if (consoleTransports instanceof Array) {
        for (let cfg of consoleTransports) {
          cfg.level = this.args.logLevel;
        }
      }
    }
    if (this.args.rebundle !== null) {
      this.config.client.webpack.rebundle = this.args.rebundle !== "no";
      this.config.client.webpack.options.watch = this.args.rebundle === "watch";
    }

    // validate some configs and apply dynamic defaults
    while (this.config.server.base.endsWith("/")) {
      this.config.server.base = this.config.server.base.slice(0, -1);
    }
    if (this.config.client.session.secret === null) {
      this.config.client.session.secret = uuidv4();
    }
    if (this.config.client.webpack.rebundle === null) {
      this.config.client.webpack.rebundle =
        process.env.NODE_ENV === "development";
    }
    if (this.config.client.webpack.options.watch === null) {
      this.config.client.webpack.options.watch =
        process.env.NODE_ENV === "development";
    }

    await this.hooks.call("server.configure.after");
  }

  setupLogging() {
    const levels = {
      all: 7,
      debug: 6,
      access: 5,
      info: 4,
      warning: 3,
      error: 2,
      fatal: 1,
      none: 0
    };
    const cfg = this.config.logging;

    // custom formatter
    const customFormatter = info => {
      const d = new Date();
      const ms = ("00" + d.getMilliseconds()).slice(-3);
      return (
        `${d.toLocaleDateString()} ${d.toLocaleTimeString()}.${ms} - ` +
        `${info.level}` +
        (info.message ? ` - ${info.message}` : "")
      );
    };

    // create winston transports based on the logging config
    const transports = [];
    for (let [type, options] of Object.entries(cfg.transports)) {
      if (!(options instanceof Array)) {
        options = [options];
      }
      for (const opts of options) {
        // special treatment of File transports
        if (type == "File") {
          if (opts.filename == null) {
            if (this.args.logFile === null) {
              continue;
            }
            opts.filename = this.args.logFile;
          }
          if (!fs.existsSync(opts.filename)) {
            fs.writeFileSync(opts.filename, "");
          }
        }
        // create the format object
        const formats = [];
        for (const type of opts.format || ["simple"]) {
          if (type == "colorize" && !util.isTTY()) {
            continue;
          } else if (type == "custom") {
            formats.push(winston.format.printf(customFormatter));
          } else {
            formats.push(winston.format[type]());
          }
        }
        opts.format = winston.format.combine(...formats);
        transports.push(new winston.transports[type](opts));
      }
    }

    // create the logger
    winston.addColors(cfg.colors);
    this.logger = winston.createLogger({
      levels: levels,
      transports: transports
    });

    // function that adds a prefixed sublogger
    this.logger.sub = prefix => {
      const logger = {};
      for (const level of Object.keys(levels)) {
        logger[level] = message => {
          this.logger[level](`[${prefix}] ${message}`);
        };
      }

      // also forward the generic log method
      logger.log = (level, message) => {
        logger[level](message);
      };

      return logger;
    };

    this.logger.info("start logging");
  }

  async setupEvents() {
    let file = this.config.server.events;
    if (file) {
      if (!path.isAbsolute(file)) {
        file = path.join(process.env.JSWITCH_BASE, file);
      }
      const events = await require(file)(this);
      this.logger.info(`setup events from file ${file}`);

      if (events) {
        this.events.onMany(events);
      }
    }
  }

  async setupHooks() {
    let file = this.config.server.hooks;
    if (file) {
      if (!path.isAbsolute(file)) {
        file = path.join(process.env.JSWITCH_BASE, file);
      }
      const hooks = await require(file)(this);
      this.logger.info(`setup hooks from file ${file}`);

      if (hooks) {
        this.hooks.merge(hooks);
      }
    }
  }

  async setupDB() {
    this.db = new DB(this);

    try {
      await this.db.setup();
    } catch (err) {
      await this.shutdown(`database connection failed, ${err}`, 1);
    }
  }

  async setupMiddleware() {
    await this.hooks.call("middleware.setup.before");

    // initialize the user auth middleware
    this.middleware.user = userMW(this);

    // initialize the permission middleware
    this.middleware.perm = permMW(this);

    await this.hooks.call("middleware.setup.after");
  }

  async setupProviders() {
    if (Object.keys(this.config.providers).length === 0) {
      this.logger.debug("no ID providers configured");
      return;
    }

    await this.hooks.call("providers.setup.before");

    for (let [name, data] of Object.entries(this.config.providers)) {
      // data might be a boolean, or an object
      if (typeof data === "boolean") {
        data = { enabled: data };
      }
      const { enabled = true, adapter = name, config = {} } = data;

      // enabled?
      if (!enabled) {
        continue;
      }

      // determine the module to load
      let module = adapter;
      try {
        require.resolve(module);
      } catch (err) {
        module = `./providers/${adapter}`;
      }

      // load the adapter class
      let adapterCls;
      try {
        adapterCls = require(module);
      } catch (err) {
        this.shutdown(
          `could not load adapter "${adapter}" for provider "${name}"`,
          1,
          err
        );
      }

      // create an instance and set it up
      await this.hooks.call(`provider.${name}.setup.before`);
      const provider = new adapterCls(this, name, config);
      await provider.setup();
      await this.hooks.call(`provider.${name}.setup.after`);

      // store it
      this.providers[name] = provider;
      this.logger.info(
        `registered ID provider "${name}" with adapter "${adapter}"`
      );
    }

    await this.hooks.call("providers.setup.after");
  }

  async setupBundling() {
    // only setup the bundling when the bundle directory is missing or rebundling is set
    const bundleExists = fs.existsSync("static/bundle");
    if (!bundleExists || this.config.client.webpack.rebundle) {
      await require("./webpack")(this);
    }
  }

  async setupApp() {
    await this.hooks.call("app.setup.before");

    this.app = new Koa();

    // basic auth
    const authCfg = this.config.server.auth;
    this.info.basicAuthEnabled = authCfg.name && authCfg.pass;
    if (this.info.basicAuthEnabled) {
      this.logger.debug(`using basic auth (${authCfg.name} : ${authCfg.pass})`);
      this.app.use(basicAuthMW(authCfg));
    } else if (authCfg.name || authCfg.pass) {
      this.logger.warning("basic auth config incomplete, skip");
    }

    // compression
    this.app.use(kCompress({ threshold: 1024 }));

    // access logs
    const accessLog = msg => {
      if (msg.endsWith("\n")) {
        msg = msg.slice(0, -1);
      }
      this.logger.access(msg);
    };
    const accessFormat = ":status :method :url :user-agent";
    this.app.use(kMorgan(accessFormat, { stream: { write: accessLog } }));

    // sessions
    this.app.keys = [this.config.client.session.secret];
    this.app.use(kSession(this.config.client.session, this.app));

    // request validation
    kValidate(this.app);

    // body parser
    this.app.use(kBodyParser());

    // error handling
    this.app.use(catchMW(this));

    // pug views
    this.app.use(
      kViews(path.join(process.env.JSWITCH_BASE, "views"), { extension: "pug" })
    );

    // create the router
    this.router = new kRouter({ prefix: this.config.server.base });

    // add the static mount
    const staticBase = this.config.server.base + "/static";
    const staticDir = path.join(process.env.JSWITCH_BASE, "static");
    this.router.get(
      "/static/*",
      staticMW(staticDir, staticBase, this.config.client.cache)
    );

    // mount routes
    require("./routes")(this, this.router);

    // mount the main router
    this.app.use(this.router.routes());
    this.app.use(this.router.allowedMethods());

    // create the actual server the app is mounted on
    const sslCfg = this.config.server.ssl;
    this.info.sslEnabled = sslCfg.key && sslCfg.cert;
    if (this.info.sslEnabled) {
      const sslOpts = {
        key: fs.readFileSync(sslCfg.key),
        cert: fs.readFileSync(sslCfg.cert)
      };
      this.server = https.createServer(sslOpts, this.app.callback());
    } else {
      if (sslCfg.key || sslCfg.cert) {
        this.logger.warning("ssl config incomplete, using http");
      }
      this.server = http.createServer(this.app.callback());
    }

    await this.hooks.call("app.setup.after");

    this.logger.info("start koa application");
  }

  async setupSocket() {
    await this.hooks.call("socket.start.before");
    this.socket = SocketIO(this.server);
    await this.hooks.call("socket.start.after");

    this.logger.info("start websocket");
  }
}

/**
 * Exports.
 */

module.exports = Server;
