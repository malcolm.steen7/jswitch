/**
 * Event handling.
 */

// npm modules
const { EventEmitter2 } = require("eventemitter2");

/**
 * Default event handler.
 */
class Events extends EventEmitter2 {
  /**
   * @param {Server} server - The server instance.
   * @param {Object} events=null - Initial event listeners, mapped to event names.
   */
  constructor(server, events = null) {
    super({ maxListeners: 50 });

    /** @member {Server} - The reference to the server instance. */
    this.server = server;

    // register passed event listeners
    if (events) {
      this.onMany(events);
    }
  }

  /**
   * Registers multiple listeners defined in a mapping of names to callbacks.
   * @param {Object} events - Event listeners, mapped to event names.
   */
  onMany(events) {
    for (let [event, listeners] of Object.entries(events)) {
      if (!(listeners instanceof Array)) {
        listeners = [listeners];
      }
      for (const listener of listeners) {
        this.on(event, listener);
      }
    }
  }
}

/**
 * Exports.
 */

module.exports = Events;
