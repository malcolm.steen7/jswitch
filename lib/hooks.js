/**
 * Hook handling.
 */

/**
 * Default hook manager.
 */
class Hooks {
  /**
   * @param {Server} server - The server instance.
   * @param {Object} hooks=null - Initial hook callbacks, mapped to hook names.
   */
  constructor(server, hooks = null) {
    /** @member {Server} - The reference to the server instance. */
    this.server = server;
    /** @member {Object} - Mapping of hook names to callbacks. */
    this.hooks = {};

    // merge passed hooks
    if (hooks) {
      this.merge(hooks);
    }
  }

  /**
   * Adds a new hook function.
   * @param {string} name - The hook name.
   * @param {Function} callback - The hook callback to register.
   */
  set(name, callback) {
    if (callback instanceof Function) {
      this.hooks[name] = callback;
    }
  }

  /**
   * Merges multiple hooks defined in a mapping of names to callbacks.
   * @param {Object} hooks - A mapping of hooks to merge into the current hooks.
   * @param {bool} overwrite=true - When true, callbacks of existing names are overwritten.
   */
  merge(hooks, overwrite = true) {
    for (let [name, callback] of Object.entries(hooks)) {
      if (overwrite || !(name in this.hooks)) {
        this.set(name, callback);
      }
    }
  }

  /**
   * Calls the callback registered for a hook and returns a promise containing its return value.
   * @param {string} name - The hook name to trigger.
   * @param {...arbitrary} args - Arguments to pass to the triggered hook callback.
   * @return {Promise} - The return value of the triggered hook callback.
   */
  async call(name, ...args) {
    // emit an event with same name and arguments
    this.server.events.emit(name, ...args);

    // call the hook and wait for completion
    if (name in this.hooks) {
      return await this.hooks[name](...args);
    }
  }
}

/**
 * Exports.
 */

module.exports = Hooks;
