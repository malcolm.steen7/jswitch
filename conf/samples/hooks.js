/**
 * User-defined hooks.
 * The jswitch server waits for hooks to complete and might also react to return values or
 * exceptions that are thrown during hook processing. There can only be one callback per hook. Each
 * hook also emits an event with identical name and arguments. However, the hook mechanism should
 * not be confused with events which rather follow a publish-subscribe paradigm.
 *
 * Current hooks (arguments):
 *   - server.setup.before
 *   - server.setup.after
 *   - server.listen.before
 *   - server.listen.after
 *   - server.configure.after
 *   - middleware.setup.before
 *   - middleware.setup.after
 *   - middleware.user.auth.after (user)
 *   - providers.setup.before
 *   - provider.${name}.setup.before
 *   - provider.${name}.setup.after
 *   - providers.setup.after
 *   - app.setup.before
 *   - app.setup.after
 *   - socket.start.before
 *   - socket.start.after
 *   - user.register.before (user)
 *   - user.register.after user)
 *   - user.login.after (user)
 *   - db.model.permission.add.before (permission)
 *   - db.model.permission.add.after (permission)
 *   - db.setup.before
 *   - db.models.setup.before
 *   - db.model.${model}.setup.before
 *   - db.model.${model}.setup.after
 *   - db.models.setup.after
 *   - db.setup.after
 *   - db.shutdown.before
 *   - db.shutdown.after
 */

module.exports = async function(server) {
  // method 1: programmatically add hooks to the server
  // example:
  // server.hooks.set("server.setup.before", async () => {
  //     ...
  // });
  // method 2: return a mapping of hook name to callback
  // example:
  // return {"server.setup.before": async () => {
  //     ...
  // }};
};
