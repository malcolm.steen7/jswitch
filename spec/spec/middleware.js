/* global describe it expect: true */

/**
 * Middleware tests.
 */

// npm modules
const request = require("request-promise-native");

// local modules
const utils = require("../helpers/utils.js");

describe("middleware", () => {
  describe("basicauth", () => {
    const options = { resolveWithFullResponse: true };

    it("should deny access without basic auth credentials", async () => {
      try {
        const res = await request.get(utils.authlessBaseURL, options);
        expect(res).toEqual("Request should fail");
      } catch (e) {
        expect(e.message).toContain("Authentication required");
      }
    });
  });
});
