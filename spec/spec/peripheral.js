/* global describe it expect beforeAll: true */

/**
 * Peripheral tests.
 */

// node modules
const path = require("path");
const fs = require("fs");

// npm modules
const uuidv4 = require("uuid/v4");

// local modules
const util = require("../../lib/util.js");

const baseDir = path.dirname(path.dirname(__dirname));

describe("package", () => {
  beforeAll(() => {
    this.pkg = JSON.parse(fs.readFileSync(path.join(baseDir, "package.json")));
  });

  it("should have the correct entry point", async () => {
    expect(this.pkg.main).toBe("index.js");
  });

  it("should not contain electron modules in devDependencies", async () => {
    for (const key of Object.keys(this.pkg.devDependencies)) {
      expect(key).not.toContain("electron");
    }
  });
});

describe("utils", () => {
  describe("userPath", () => {
    it("userPath should expand home", () => {
      const withTilde = util.userPath("~/test");
      expect(withTilde).toEqual(`${process.env.HOME}/test`);
      const withEnv = util.userPath("~/test");
      expect(withEnv).toEqual(`${process.env.HOME}/test`);
    });
  });

  describe("readConfigSync", () => {
    it("should allow for optional reading without parsing", () => {
      const srcFile = `/tmp/test_${uuidv4()}`;
      const content = "test\n";
      fs.writeFileSync(srcFile, content, "utf8");
      expect(util.readConfigSync(srcFile, false)).toEqual(content);
      fs.unlinkSync(srcFile);
    });
  });

  describe("copyFileSync", () => {
    it("should be able to copy a file into a directory", () => {
      const srcFile = `/tmp/test_${uuidv4()}`;
      const dstDir = `/tmp/test_${uuidv4()}`;
      const content = "test\n";

      fs.writeFileSync(srcFile, content, "utf8");
      fs.mkdirSync(dstDir);

      const dstFile = util.copyFileSync(srcFile, dstDir);
      expect(fs.existsSync(dstFile)).toBe(true);
      expect(fs.readFileSync(dstFile, "utf8")).toEqual(content);

      fs.unlinkSync(dstFile);
      fs.unlinkSync(srcFile);
      fs.rmdirSync(dstDir);
    });

    it("should overwrite an existing file if wanted", () => {
      const srcFile = `/tmp/test_${uuidv4()}`;
      const dstFile = `/tmp/test_${uuidv4()}`;
      const content = "test\n";
      const oldContent = "old\n";

      fs.writeFileSync(srcFile, content, "utf8");
      fs.writeFileSync(dstFile, oldContent, "utf8");

      util.copyFileSync(srcFile, dstFile, true);
      expect(fs.readFileSync(dstFile, "utf8")).toEqual(content);

      fs.unlinkSync(srcFile);
      fs.unlinkSync(dstFile);
    });

    it("should not overwrite an existing file it not wanted", () => {
      const srcFile = `/tmp/test_${uuidv4()}`;
      const dstFile = `/tmp/test_${uuidv4()}`;
      const content = "test\n";
      const oldContent = "old\n";

      fs.writeFileSync(srcFile, content, "utf8");
      fs.writeFileSync(dstFile, oldContent, "utf8");

      expect(util.copyFileSync(srcFile, dstFile)).toBe(null);
      expect(fs.readFileSync(dstFile, "utf8")).toEqual(oldContent);

      fs.unlinkSync(srcFile);
      fs.unlinkSync(dstFile);
    });
  });

  describe("createSymlinkSync", () => {
    it("should create a symlink in a directory", () => {
      const srcFile = `/tmp/test_${uuidv4()}`;
      const dstDir = `/tmp/test_${uuidv4()}`;
      const content = "test\n";

      fs.writeFileSync(srcFile, content, "utf8");
      fs.mkdirSync(dstDir);

      const dstFile = util.createSymlinkSync(srcFile, dstDir);
      expect(fs.existsSync(dstFile)).toBe(true);
      expect(fs.readFileSync(dstFile, "utf8")).toEqual(content);

      fs.unlinkSync(dstFile);
      fs.unlinkSync(srcFile);
      fs.rmdirSync(dstDir);
    });
  });

  describe("respond", () => {
    it("should stringify data and add it to the context body", () => {
      const ctx = { body: "", header: { "content-type": "" } };
      const json = { test: true };
      util.respond(ctx, json);
      expect(ctx.body).toContain(JSON.stringify(json));
    });
  });

  describe("PermissionError", () => {
    it("should provide a meaningful message", () => {
      const error = new util.PermissionError(
        { name: "TestUser" },
        "stop testing"
      );
      try {
        throw error;
      } catch (e) {
        expect(e.message).toContain("TestUser");
        expect(e.message).toContain("permission");
        expect(e.message).toContain("stop testing");
      }
    });
  });
});

describe("hooks", () => {
  for (const hookName of Object.keys(global.hookFlags)) {
    it(`${hookName} should be called`, () => {
      expect(global.hookFlags[hookName]).toBe(true);
    });
  }
});
