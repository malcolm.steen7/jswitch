/* global describe it expect beforeAll: true */

/**
 * Page tests.
 */

// npm modules
const request = require("request-promise-native");

// local modules
const utils = require("../helpers/utils.js");

describe("public static pages routes", () => {
  it("should serve /login", async () => {
    const res = await request.get(`${utils.baseURL}/login`, {
      resolveWithFullResponse: true
    });
    expect(res.statusCode).toEqual(200);
    expect(res.body).toContain('<html lang="en">');
  });

  it("should redirect to /login from / and /index", async () => {
    const login = await request.get(`${utils.baseURL}/login`, {
      resolveWithFullResponse: true
    });
    for (let endpoint of ["", "index"]) {
      const redirect = await request.get(`${utils.baseURL}/${endpoint}`, {
        resolveWithFullResponse: true
      });
      expect(redirect.statusCode).toEqual(200);
      expect(redirect.body).toEqual(login.body);
    }
  });
});

describe("private static page routes", () => {
  const cookieJar = request.jar();
  const options = {
    jar: cookieJar,
    resolveWithFullResponse: true
  };

  beforeAll(async () => {
    await utils.loginFixtureUser(cookieJar);
  });

  it("should serve /index at /", async () => {
    const index = await request.get(`${utils.baseURL}/index`, options);
    const slash = await request.get(`${utils.baseURL}/index`, options);
    const wrong = await request.get(`${utils.baseURL}/index`, {
      resolveWithFullResponse: true
    });

    expect(index.statusCode).toEqual(200);
    expect(slash.statusCode).toEqual(200);
    expect(index.body).toEqual(slash.body);
    expect(index.body).not.toEqual(wrong.body);
  });
});
